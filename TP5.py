#! /usr/bin/env python
# -*- coding: utf-8 -*-
import cv2
import numpy as np

#Se Lee la imagen
img_path = "ej05.png"
img = cv2.imread(img_path)  
cache = img.copy()
orig = img.copy()

#se declaran variables
drawing = False
ix , iy = -1 , -1
pos_x = [0,0]
pos_y = [0,0]

def draw_circle (event , x , y , flags , param ) :
    global ix , iy , drawing , mode, img, cache, pos_x, pos_y
    #Si un boton es presionado reinicio la imagen a mostrar y el caché
    if event == cv2.EVENT_LBUTTONDOWN:
        img = orig.copy()
        cache = orig.copy()
        drawing = True
        ix , iy = x , y
    #Si estoy moviendo el mouse que la imagen sea la tomada por el caché (la original) y la del caché pasa a la imagen
    elif event == cv2.EVENT_MOUSEMOVE:
        if drawing is True :
            img = cache.copy()
            cache = img.copy()
            cv2.rectangle(img, (ix,iy), (x,y), (0,255,0), 1)    #Si estoy presionando el click que dibuje un rectangulo
        else:
            img = cache.copy()
            cache = img.copy()
            cv2.circle(img, (x,y), 5, (0,0,255), -1)    #Si no lo estoy haciendo que me dibuje un circulo en la posicion del mouse

    elif event == cv2 .EVENT_LBUTTONUP:
        #Al soltar el mouse que se dibuje un rectangulo en las posiciones marcadas y se devuelven las posiciones de inicio y fin del rectangulo
        drawing = False
        cv2.rectangle(img, (ix,iy), (x,y), (0,0,255), 2)
        cache = img.copy()
        pos_x = [ix,x]
        pos_y = [iy,y]
        

cv2.namedWindow('imagen')
cv2.setMouseCallback('imagen', draw_circle)
while(1):
    cv2.imshow('imagen', img)
    k = cv2.waitKey(1) & 0xFF
    if k == ord('m'):
        mode = not mode
    elif k == 27 :
        break
    elif k == ord('g'):
        cv2.imwrite('ej5_out.png', orig[min(pos_y):max(pos_y), min(pos_x):max(pos_x),:])  #Se guarda la imagen en la posicion deseada
    elif k == ord('r'):
        img = orig.copy()
        cache = orig.copy()
        pos_x=[0,0]
        pos_y=[0,0]
    elif k == ord('e'):
        img_path2 = "ej5_out.png"
        img2 = cv2.imread(img_path2)
        #cv2.imshow('imagen', img2)
        (h,w)= (img2.shape[0],img2.shape[1])
        #(h,w)= (img.shape[0],img.shape[1])
        tx=float(input("ingrese la traslacion en x: "))
        ty=float(input("ingrese la traslacion en y: "))
        ang=int(input("ingrese el angulo: "))
        center=(w/2,h/2)  
        #rotacion
        M=cv2.getRotationMatrix2D(center,ang,1.0)
        trasladada=cv2.warpAffine(img2,M,(h,w))
        #traslacion
        N=np.float32([[1,0,tx],[0,1,ty]])
        tras=cv2.warpAffine(trasladada,N,(h,w))
        cv2.imshow('imagen', tras)
        cv2.imwrite('transformacion05.png',tras)  #Guardo la imagen
    elif k == ord('q'):
        break
cv2.destroyAllWindows()
