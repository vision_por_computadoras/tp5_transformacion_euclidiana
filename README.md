# TP5_Transformacion_Euclidiana



#Nota 

Primero se selecciona la parte, luego se guarda con g y a ese resultado, cuando se apreta e se le realiza la transformacion. Con q o esc se sale.

#Consigna

Crear una función que aplique una transformación euclidiana, recibiendo los siguientes parámetros:
Parámetros
• angle: Ángulo
• tx: traslación en x
• ty: traslación en y

Escribir un programa que permita seleccionaruna porción rectangular de una imagen y con la letra “e” aplique una transformación euclidiana a la porción de imagen seleccionada (solicitando al usuario los tres parámetros, ángulo, traslación en x y
traslación en y) y la guarde como una nueva imagen.